import gui.functional.NeuralNetwork;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Runner {
    public static void main(String[] args) {
//        MainFrameController controller = new MainFrameController();
//        controller.start();
        File folder = new File("C:\\Users\\Siblion1\\IdeaProjects\\NeuroNetwork\\src\\main\\resources\\screen");
        File[] listOfFiles = folder.listFiles();
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        int[][] weight = new int[30][30];

        for (int i = 0; i < listOfFiles.length; i++) {
            try {
                for (String s : neuralNetwork.wordArray) {
                    if (listOfFiles[i].getName().contains(s)) {
                        BufferedImage imageIO = ImageIO.read(listOfFiles[i]);
                        for (int x = 0; x < 30; x++) {
                            for (int y = 0; y < 30; y++) {
                                weight[x][y] = imageIO.getRGB(x, y);
                            }
                        }
                        neuralNetwork.study(weight, s);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        neuralNetwork.prepareForSerialization();
    }
}
