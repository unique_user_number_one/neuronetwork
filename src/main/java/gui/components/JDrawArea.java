package gui.components;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class JDrawArea extends JComponent {

    private Image image;

    private Graphics2D g2;

    private int currentX, currentY, oldX, oldY;

    public JDrawArea() {
        setDoubleBuffered(false);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                oldX = e.getX();
                oldY = e.getY();
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();

                if (g2 != null) {
                    g2.drawLine(oldX, oldY, currentX, currentY);
                    repaint();
                    oldX = currentX;
                    oldY = currentY;
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (image == null) {
            image = createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) image.getGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            BasicStroke pen1 = new BasicStroke(20);
            g2.setStroke(pen1);
            clear();
        }
        g.drawImage(image, 0, 0, null);
    }

    public void clear() {
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, getSize().width, getSize().height);
        g2.setPaint(Color.black);
        repaint();
    }

    public void setWidthLines(int widthLines) {
        BasicStroke pen1 = new BasicStroke(widthLines);
        g2.setStroke(pen1);
    }

    int i = 0;

    public void saveScreen() {
        BufferedImage bufferedImage = toBufferedImage(image);
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-hh-mm-ss");
            File file = new File("C:/Users/Siblion1/IdeaProjects/NeuroNetwork/src/main/resources/screen/" + /*LocalDateTime.now().format(formatter).toString()*/ "F" + i + ".png");
            file.createNewFile();
            ImageIO.write(bufferedImage, "png", file);
            i++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BufferedImage toBufferedImage(Image img) {

        BufferedImage bimage = new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB);

        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, 30, 30, null);
        bGr.dispose();
        return bimage;
    }
}
