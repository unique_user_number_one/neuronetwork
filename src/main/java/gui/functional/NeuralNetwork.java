package gui.functional;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class NeuralNetwork {

    public Neuron[] neurons;

    public String[] wordArray;

    public void initWord() {

        Set<String> words = new HashSet<>();
        File folder = new File("C:\\Users\\Siblion1\\IdeaProjects\\NeuroNetwork\\src\\main\\resources\\screen");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                words.add(String.valueOf(listOfFiles[i].getName().charAt(0)));
            }
        }
        wordArray = words.toArray(new String[words.size()]);
    }

    public NeuralNetwork() {
        initWord();
        neurons = new Neuron[wordArray.length];
        for (int i = 0; i < wordArray.length; i++) {
            neurons[i] = new Neuron();
            neurons[i].word = wordArray[i];
        }
    }

    int[] handleHard(int[][] input) {
        int[] output = new int[neurons.length];
        for (int i = 0; i < output.length; i++)
            output[i] = neurons[i].transferHard(input);
        return output;
    }

    int[] handle(int[][] input) {
        int[] output = new int[neurons.length];
        for (int i = 0; i < output.length; i++)
            output[i] = neurons[i].transfer(input);

        return output;
    }

    public int getAnswer(int[][] input) {
        int[] output = handle(input);
        int maxIndex = 0;
        for (int i = 1; i < output.length; i++)
            if (output[i] > output[maxIndex])
                maxIndex = i;

        return maxIndex;
    }

    public void study(int[][] input, String correctAnswer) {
        for(Neuron n : neurons){
            if(n.word.equals(correctAnswer)){
                n.changeWeights(input);
            }
        }





//        int[] correctOutput = new int[neurons.length];
//        correctOutput[correctAnswer] = 1;
//        int[] output = handleHard(input);
//        while (!compareArrays(correctOutput, output)) {
//            for (int i = 0; i < neurons.length; i++) {
//                int dif = correctOutput[i] - output[i];
//                neurons[i].changeWeights(input, dif);
//            }
//            output = handleHard(input);
//        }
    }

    boolean compareArrays(int[] a, int[] b) {
        if (a.length != b.length)
            return false;
        for (int i = 0; i < a.length; i++)
            if (b[i] - a[i] > 65)
                return false;
        return true;
    }

    public void prepareForSerialization() {
        for (Neuron n : neurons)
            n.prepareForSerialization();
    }

    public void onDeserialize() {
        for (Neuron n : neurons)
            n.onDeserialize();
    }
}
