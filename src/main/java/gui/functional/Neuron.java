package gui.functional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Neuron {

    public String word;
    public int[][] weight;
    public BufferedImage input;

    public static final int pixelX = 30;
    public static final int pixelY = 30;

    public static final int minimum = 50;

    public Neuron() {
        initWeight();
    }

    private void initWeight(){
        weight = new int[pixelX][pixelY];
        for(int x = 0; x < pixelX; x++){
            for(int y = 0; y < pixelY; y++){
                weight[x][y] = -1;
            }
        }
    }

    public int transferHard(int[][] input) {
        int power = 0;
        for (int i = 0; i < pixelX; i++) {
            for (int c = 0; c < pixelY; c++) {
                power += weight[i][c] * input[i][c];
            }
        }
        return power >= minimum ? 1 : 0;
    }

    public int transfer(int[][] input) {
        int power = 0;
        for (int i = 0; i < pixelX; i++) {
            for (int c = 0; c < pixelY; c++) {
                power += weight[i][c] * input[i][c];
            }
        }
        return power;
    }
//
//    public void changeWeights(int[][] input, int d) {
//        for (int r = 0; r < pixelX; r++)
//            for (int c = 0; c < pixelY; c++)
//                weight[r][c] += d * input[r][c];
//    }

    public void changeWeights(int[][] input) {
        for (int r = 0; r < pixelX; r++) {
            for (int c = 0; c < pixelY; c++) {
                if (weight[r][c] > input[r][c]) {
                    weight[r][c] = input[r][c];
//                    System.out.print(input[r][c] + " ");
                }
            }
//            System.out.println();
        }
    }

    public void prepareForSerialization() {
        BufferedImage img = new BufferedImage(pixelX, pixelY, BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < pixelX; x++) {
            for (int y = 0; y < pixelY; y++) {
                img.setRGB(x, y, weight[x][y]);
            }
        }
        try {
            File f = new File("C:\\Users\\Siblion1\\IdeaProjects\\NeuroNetwork\\src\\main\\resources\\result\\" + "neuron" + word + ".png");
            ImageIO.write(img, "png", f);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void onDeserialize() {
        weight = new int[pixelX][pixelY];
        for (int i = 0; i < pixelX; i++) {
            for (int j = 0; j < pixelY; j++) {
                weight[i][j] = input.getRGB(i, j);
            }
        }
    }
}
