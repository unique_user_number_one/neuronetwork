package gui.view;

import gui.components.JDrawArea;

import javax.swing.*;
import java.awt.*;

public class Main extends JFrame {
    private JSlider slider;
    private JButton educateButton;
    private JButton resultButton;
    private JTextField resultField;
    private JTextField educateField;
    private JPanel drawingPanel;
    private JPanel mainPanel;
    private JScrollPane scrollPanel;
    private JButton clearButton;
    private JDrawArea drawArea;

    public Main() {
        setSize(1000, 400);
        setVisible(true);
        setContentPane(mainPanel);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        drawArea = new JDrawArea();
    }

    public JSlider getSlider() {

        slider.setValue(20);
        slider.setMinimum(20);
        slider.setMaximum(40);
        return slider;
    }


    public JButton getEducateButton() {
        return educateButton;
    }

    public JButton getResultButton() {
        return resultButton;
    }

    public JTextField getResultField() {
        return resultField;
    }

    public JTextField getEducateField() {
        return educateField;
    }

    public JPanel getDrawingPanel() {
        drawingPanel.setLayout(new BorderLayout());
        drawingPanel.add(drawArea);
        return drawingPanel;
    }

    public JScrollPane getScrollPanel() {
        return scrollPanel;
    }

    public JButton getClearButton() {
        return clearButton;
    }

    public JDrawArea getDrawArea() {
        return drawArea;
    }
}
