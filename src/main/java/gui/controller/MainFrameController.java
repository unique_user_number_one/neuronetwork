package gui.controller;

import gui.components.JDrawArea;
import gui.view.Main;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrameController {

    private Main mainFrame;
    private JSlider slider;
    private JButton educateButton;
    private JButton resultButton;
    private JButton clearButton;
    private JTextField resultField;
    private JTextField educateField;
    private JPanel drawingPannel;
    private JScrollPane scrollPanel;
    private JDrawArea drawArea;


    public MainFrameController() {
        initComponent();
        initListeners();
    }

    private void initComponent() {
        mainFrame = new Main();
        slider = mainFrame.getSlider();
        drawArea = mainFrame.getDrawArea();
        educateButton = mainFrame.getEducateButton();
        resultButton = mainFrame.getResultButton();
        resultField = mainFrame.getResultField();
        educateField = mainFrame.getEducateField();
        drawingPannel = mainFrame.getDrawingPanel();
        scrollPanel = mainFrame.getScrollPanel();
        clearButton = mainFrame.getClearButton();
    }

    public void initListeners() {
        resultButton.addActionListener(new TestListener());
        educateButton.addActionListener(new ListenerTest());
        clearButton.addActionListener(new ClearDrawArea());
        slider.addChangeListener(new ChangeSlider());
    }

    public void start() {
        mainFrame.setVisible(true);
    }

    private class TestListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            resultField.setText("ehu");
            drawArea.saveScreen();
        }
    }

    private class ListenerTest implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String[] headers = {"Буквы", "%"};
            String[][] rows = {{"А", "5.12"}, {"Б", "99.55"}, {"Ю", "4.0"}, {"Г", "89.9"}};
            JTable table = new JTable(rows, headers);
            scrollPanel.setViewportView(table);
        }
    }

    private class ClearDrawArea implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            drawArea.clear();
        }
    }

    private class ChangeSlider implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            drawArea.setWidthLines(((JSlider) e.getSource()).getValue());
        }
    }
}
